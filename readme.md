# prosearch
prosearch lets you surf the web and choose the search engine you want to use, like:
- Google
- Bing
- Yahoo
- DuckDuckGo
- Yandex
- Qwant
- Ecosia
- Dogpile
- Webopedia

> If you want to add a search engine, submit an issue or a pull request

<h3>This project was made with <a href="https://vitejs.dev">Vite!</a></h3>