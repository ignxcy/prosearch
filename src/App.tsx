import './App.css'

function App() {
  return (
    <>
      <div>
        <h1>prosearch</h1>
        <p>surf the web</p>
      </div>
      <div>
        <form action="https://www.google.com/search">
          <input type="search" name="q" id="google" placeholder='Google' className="input"/>
        </form>
        <form action="https://www.bing.com/search">
          <input type="search" name="q" id="bing" placeholder='Bing' className="input"/>
        </form>
        <form action="https://search.yahoo.com/search">
          <input type="search" name="p" id="yahoo" placeholder='Yahoo' className="input"/>
        </form>
        <form action="https://duckduckgo.com/">
          <input type="search" name="q" id="duckduckgo" placeholder='DuckDuckGo' className="input"/>
        </form>
        <form action="https://yandex.com/search/">
          <input type="search" name="text" id="yandex" placeholder='Yandex' className="input"/>
        </form>
        <form action="https://search.brave.com/search">
          <input type="search" name="q" id="webopedia" placeholder='Brave' className="input"/>
        </form>
        <form action="https://qwant.com/">
          <input type="search" name="q" id="qwant" placeholder='Qwant' className="input"/>
        </form>
        <form action="https://ecosia.org/search">
          <input type="search" name="q" id="ecosia" placeholder='Ecosia' className="input"/>
        </form>
        <form action="https://www.dogpile.com/serp">
          <input type="search" name="q" id="dogpile" placeholder='Dogpile' className="input"/>
        </form>
        <form action="https://webopedia.com/">
          <input type="search" name="s" id="webopedia" placeholder='Webopedia' className="input"/>
        </form>
      </div>
    </>
  )
}

export default App